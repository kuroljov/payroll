// @flow
'use strict'

const t = require('tap')
const Parser = require('../../src/lib/Parser')
const WorkEvent = require('../../src/models/WorkEvent')

t.test('Parser', t => {
  t.test('validate', t => {
    t.test('ok', t => {
      const body = {
        employee: 'Michael Jacobs',
        company: 'Awesome Company',
        from: { year: 2017, month: 5, day: 1 },
        to: { year: 2017, month: 6, day: 2 }
      }
      const result = Parser.validate(body)

      t.ok(result.ok)
      t.notOk(result.error)
      t.ok(Array.isArray(result.errors))
      t.equal(result.errors.length, 0)

      t.end()
    })

    t.test('lack', t => {
      const body = {
        employee: { bananas: false },
        company: undefined,
        from: [1, 2, 3, [[[[[]]]]]],
        to: new Date()
      }
      const result = Parser.validate(body)

      t.ok(result.error)
      t.notOk(result.ok)
      t.ok(Array.isArray(result.errors))
      t.equal(result.errors.length, 4)
      t.ok(result.errors.every(x => x instanceof TypeError))

      t.equal(result.errors[0].message, 'employee as a string is expected')
      t.equal(result.errors[1].message, 'company as a string is expected')
      t.equal(result.errors[2].message, 'from as a valid day structure is expected')
      t.equal(result.errors[3].message, 'to as a valid day structure is expected')

      t.end()
    })

    t.end()
  })

  t.test('with excludes', t => {
    const body: Object = {
      employee: 'Michael Jacobs',
      company: 'Awesome Company',
      from: { year: 2017, month: 6, day: 9 },
      to: { year: 2017, month: 7, day: 9 },
      exclude: [
        { year: 2017, month: 6, day: [26, 27, 28, 29, 30] },
        { year: 2017, month: 7, day: [1, 2] }
      ]
    }
    const weekends: Array<Object> = [
      { year: 2017, month: 6, day: [10, 11, 17, 18, 24, 25] },
      { year: 2017, month: 7, day: [1, 2, 8, 9] }
    ]
    const bankHolidays: Array<Object> = [
      { year: 2017, month: 6, day: [23, 24] }
    ]
    const workDays: Array<number> = [
      9, 12, 13, 14, 15, 16, 19, 20, 21, 22,
      3, 4, 5, 6, 7
    ]
    const workDaysTotal: number = workDays.length
    const events: Array<WorkEvent> = Parser.parse(body, [
      ...weekends,
      ...bankHolidays
    ])

    t.equal(events.length, workDaysTotal)
    t.equal(events.filter(event => event instanceof WorkEvent).length, workDaysTotal)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 9).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 12).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 13).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 14).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 15).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 16).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 19).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 20).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 21).length, 1)
    t.equal(events.filter(({ date }) => date.month === 6 && date.day === 22).length, 1)
    t.equal(events.filter(({ date }) => date.month === 7 && date.day === 3).length, 1)
    t.equal(events.filter(({ date }) => date.month === 7 && date.day === 4).length, 1)
    t.equal(events.filter(({ date }) => date.month === 7 && date.day === 5).length, 1)
    t.equal(events.filter(({ date }) => date.month === 7 && date.day === 6).length, 1)
    t.equal(events.filter(({ date }) => date.month === 7 && date.day === 7).length, 1)

    t.end()
  })

  t.test('without excludes', t => {
    const body: Object = {
      employee: 'Michael Jacobs',
      company: 'Awesome Company',
      from: { year: 2017, month: 12, day: 30 },
      to: { year: 2018, month: 1, day: 2 }
    }
    const workDays: Array<number> = [
      30, 31,
      1, 2
    ]
    const workDaysTotal: number = workDays.length
    const events: Array<WorkEvent> = Parser.parse(body)

    t.equal(events.length, workDaysTotal)
    t.equal(events.filter(event => event instanceof WorkEvent).length, workDaysTotal)
    t.equal(events.filter(({ date }) =>
      date.year === 2017 &&
      date.month === 12 &&
      date.day === 30
    ).length, 1)
    t.equal(events.filter(({ date }) =>
      date.year === 2017 &&
      date.month === 12 &&
      date.day === 31
    ).length, 1)
    t.equal(events.filter(({ date }) =>
      date.year === 2018 &&
      date.month === 1 &&
      date.day === 1
    ).length, 1)
    t.equal(events.filter(({ date }) =>
      date.year === 2018 &&
      date.month === 1 &&
      date.day === 2
    ).length, 1)

    t.end()
  })

  t.end()
})
