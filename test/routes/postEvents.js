// @flow
'use strict'

const t = require('tap')
const sinon = require('sinon')
const postEvents = require('../../src/routes/postEvents')
const WorkEvent = require('../../src/models/WorkEvent')

t.test('postEvents', t => {
  t.test('ok', t => {
    const query = {
      employee: 'Michael Jacobs',
      company: 'Awesome company',
      from: { year: 2017, month: 6, day: 1 },
      to: { year: 2017, month: 6, day: 6 },
      exclude: [
        { year: 2017, month: 6, day: [1, 2] }
      ]
    }
    const x = {
      url: '/events',
      method: 'POST',
      status: 404,
      request: {
        body: query
      }
    }
    const state = new Map()
    const next = sinon.stub()

    state.set('excludeList', [
      { year: 2017, month: 6, day: [1, 2, 3, 16, 17, 22, 31] }
    ])

    const response = postEvents(state)(x, next)

    t.equal(next.callCount, 0)
    t.equal(response.status, 200)
    t.equal(x.status, 200)
    t.ok(response.ok)
    t.ok(response.data.length, 3)
    t.ok(response.data.filter(x => x instanceof WorkEvent).length, 3)

    t.end()
  })

  t.test('invalid query', t => {
    const query = {
      employee: 'Michael Jacobs',
      company: 'Awesome company',
      from: undefined,
      to: { year: 2017, month: 6, day: 6 },
      exclude: [
        { year: 2017, month: 6, day: [1, 2] }
      ]
    }
    const x = {
      url: '/events',
      method: 'POST',
      status: 404,
      request: {
        body: query
      }
    }
    const next = sinon.stub()

    const response = postEvents(new Map())(x, next)

    t.equal(next.callCount, 0)
    t.equal(response.status, 400)
    t.equal(x.status, 400)
    t.equal(response.error, true)
    t.notOk(response.data)
    t.equal(response.errors.length, 1)

    t.end()
  })

  t.test('call next', t => {
    const x = {
      url: '/events-or-not',
      method: 'POST'
    }
    const next = sinon.stub()

    postEvents(new Map())(x, next)

    t.equal(next.callCount, 1)

    t.end()
  })

  t.end()
})
