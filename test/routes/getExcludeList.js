// @flow
'use strict'

const t = require('tap')
const sinon = require('sinon')
const getExcludeList = require('../../src/routes/getExcludeList')
const Day = require('../../src/lib/Day')

t.test('getExcludeList', t => {
  t.test('ok', t => {
    const x = {
      method: 'GET',
      url: '/excludelist'
    }
    const state = new Map()
    const next = sinon.stub()

    state.set('excludeList', [new Day({ year: 2017, month: 6, day: 13 })])

    const response = getExcludeList(state)(x, next)

    t.equal(next.callCount, 0)
    t.deepEqual(response, state.get('excludeList'))

    t.end()
  })

  t.test('call next', t => {
    const x = {
      method: 'GET',
      url: '/excludelist-invalid-url'
    }
    const next = sinon.stub()

    getExcludeList(new Map())(x, next)

    t.equal(next.callCount, 1)

    t.end()
  })

  t.end()
})
