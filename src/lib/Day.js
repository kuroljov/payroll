// @flow
'use strict'

type year = number
type month = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
type day = month | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 |
  24 | 25 | 26 | 27 | 28 | 29 | 30 | 31
type args = { year: year, month: month, day: day }

class Day {
  year: year
  month: month
  day: day

  constructor (args: args) {
    if (!Day.isValidOne(args)) {
      throw new Error('invalid args')
    }

    this.year = args.year
    this.month = args.month
    this.day = args.day
  }

  addYear (n?: number = 1): void {
    this.year += n
  }

  addMonth (n?: number = 1): void {
    if (this.month + n > 12) {
      this.addYear()
      this.month = 1
    } else {
      this.month += n
    }
  }

  addDay (n?: number = 1): void {
    const limit: number = Day.monthLength(this.year, this.month)

    if (this.day + n > limit) {
      this.addMonth()
      this.day = 1
    } else {
      this.day += n
    }
  }

  equals (day: Day): boolean {
    return this.year === day.year &&
      this.month === day.month &&
      this.day === day.day
  }

  toString (): string {
    return `${this.year}-${this.month}-${this.day}`
  }

  static monthLength (year: year, month: month): number {
    return new Date(year, month, 0).getDate()
  }

  static equals (a: Day, b: Day): boolean {
    return a.year === b.year &&
      a.month === b.month &&
      a.day === b.day
  }

  static clone (base: Day): Day {
    return new Day({
      year: base.year,
      month: base.month,
      day: base.day
    })
  }

  static split (subject: Object): Array<Day> {
    if (!Day.isValid(subject)) {
      return []
    }

    if (!Array.isArray(subject.day)) {
      return [new Day(subject)]
    }

    return subject.day.map(day => new Day({
      year: subject.year,
      month: subject.month,
      day
    }))
  }

  static isValid (subject: any): boolean {
    if (typeof subject !== 'object') {
      return false
    }

    const { year, month, day } = subject || {}

    return (
      Day.isValidDay(day) ||
      (Array.isArray(day) && day.length && day.every(Day.isValidDay))
    ) &&
      Day.isValidMonth(month) &&
      Day.isValidYear(year)
  }

  static isValidOne (subject: any): boolean {
    if (typeof subject !== 'object') {
      return false
    }

    const { year, month, day } = subject || {}

    return Day.isValidDay(day) &&
      Day.isValidMonth(month) &&
      Day.isValidYear(year)
  }

  static isValidYear (x: any): boolean {
    return typeof x === 'number'
  }

  static isValidMonth (x: any): boolean {
    return typeof x === 'number' && x >= 1 && x <= 12
  }

  static isValidDay (x: any): boolean {
    return typeof x === 'number' && x >= 1 && x <= 31
  }
}

module.exports = Day
