// @flow
'use strict'

const crypto = require('crypto')

function getId (length: number = 8) {
  return crypto.randomBytes(length).toString('hex')
}

module.exports = getId
