// @flow
'use strict'

const Day = require('./Day')
const WorkEvent = require('../models/WorkEvent')

function validate (body: Object = {}): Object {
  const response = {
    ok: true,
    error: false,
    errors: []
  }

  if (typeof body.employee !== 'string') {
    const error = new TypeError(expect('employee', 'string'))
    response.errors.push(error)
  }

  if (typeof body.company !== 'string') {
    const error = new TypeError(expect('company', 'string'))
    response.errors.push(error)
  }

  if (!Day.isValidOne(body.from)) {
    const error = new TypeError(expect('from', 'valid day structure'))
    response.errors.push(error)
  }

  if (!Day.isValidOne(body.to)) {
    const error = new TypeError(expect('to', 'valid day structure'))
    response.errors.push(error)
  }

  if (response.errors.length) {
    response.ok = false
    response.error = true
  }

  return response

  function expect (el: string, expected: string): string {
    return `${el} as a ${expected} is expected`
  }
}

function parse (body: Object, exclude?: Array<Day> = []): Array<WorkEvent> {
  const arr: Array<WorkEvent> = []
  const excludes: Array<Day> = [].concat(body.exclude, exclude)
  const excludesTable = {}

  // split, sort and merge all excludes
  for (let n = 0, len = excludes.length; n < len; n++) {
    const ex: Day = excludes[n]
    const splits: Array<Day> = Day.split(ex)

    for (let i = 0, len = splits.length; i < len; i++) {
      const split: Day = splits[i]
      const key: string = split.toString()

      if (!excludesTable[key]) {
        excludesTable[key] = split
      }
    }
  }

  const cursor: Day = Day.split(body.from)[0]
  const destDay: Day = Day.split(body.to)[0]

  // Very last day has to be included as well
  destDay.addDay()

  while (!Day.equals(cursor, destDay)) {
    if (!excludesTable[cursor.toString()]) {
      arr.push(new WorkEvent({
        company: body.company,
        employee: body.employee,
        date: Day.clone(cursor)
      }))
    }
    cursor.addDay()
  }

  return arr
}

module.exports = {
  parse,
  validate
}
