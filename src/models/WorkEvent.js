// @flow
'use strict'

const getId = require('../lib/getId')
const Day = require('../lib/Day')

type Constructor = {
  company: string,
  employee: string,
  date: Day,
  size?: number
}

class WorkEvent {
  id: string
  company: string
  employee: string
  date: Day
  size: number
  createdAt: number

  constructor (config: Constructor) {
    this.id = getId()
    this.company = config.company
    this.employee = config.employee
    this.date = config.date
    this.size = config.size || 8
    this.createdAt = Date.now()
  }
}

module.exports = WorkEvent
